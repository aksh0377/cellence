<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Awesome Theme</title>
		<?php wp_head(); ?>
	</head>
	
	<?php 
		
		if( is_front_page() ):
			$awesome_classes = array( 'awesome-class', 'my-class' );
		else:
			$awesome_classes = array( 'no-awesome-class' );
		endif;
		
	?>
	
	<body <?php body_class( $awesome_classes ); ?>>

		
		<div class="container">
		
			<div class="row">
				
				
					
<nav class="navbar navb">
  <div class="container-fluid row">
    <div class="navbar-header" 
					      <button type="button" class="navbar-toggle " data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					        <!-- <span class="sr-only">Toggle navigation</span> -->
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					      </button>
					      <a class="navbar-brand" ><img src="../wp-content/themes/awesome/images/logo_03.png" alt="cellence logo" >
					      </a>
                           
					    </div >
					    <div class="nav-navbar navbar-header">
					            <ul class="collapse navbar-collapse " id="bs-example-navbar-collapse-1" >
					    	    <?php 
								wp_nav_menu(array(
									'theme_location' => 'primary',
									'menu_class' => 'pager nav nav-navbar'
									)
								  );
							    ?></ul>
						</div>
						  <div id="contact-header" class=" nav nav-navbar navbar-right "> 
							<ul class=" nav nav-navbar">
							    <li class="nav navbar-right"><h2 >0845 505050</h2></li>
					            <li class=" nav navbar-right" id="phone"><img src="../wp-content/themes/awesome/images/design_05.png" alt="cellence logo" ></li>
					        </ul>
					        <ul class="row nav  nav-navbar pager navbar-right nav-header">
					        <li > <a href="#" id="grad" >Sign Up</a></li>
					        <li id="sign"><a href="#"  id="grad">Login</a></li>
					        </ul>
						 </div>
						
	 </div>					
	</div><!-- /.container-fluid -->
</nav>
				
				<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="../wp-content/themes/awesome/images/design_09.png" alt="slider 1">
      <div class="carousel-caption">
        ...
      </div>
    </div>
    <div class="item">
      <img src="../wp-content/themes/awesome/images/design_09.png" alt="slider 2">
      <div class="carousel-caption">
        ...
      </div>
    </div>
    ...
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
	
			